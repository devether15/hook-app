import React from 'react';
import ReactDOM from 'react-dom';
import { CallbackHook } from './Components/06-memos/CallbackHook';
// import { MemoHook } from './Components/06-memos/MemoHook';
// import { FormWithCustomHook } from './Components/02-useEffect/FormWithCustomHook';
// import { MultipleCustomHooks } from './Components/03-examples/MultipleCustomHooks';
// import { FocusScreen } from './Components/04-useRef/FocusScreen';
// import { RealExampleRef } from './Components/04-useRef/RealExampleRef';
// import { LayoutEffect } from './Components/05-useLayoutEffect/LayoutEffect';
// import { SimpleForm } from './Components/01-useState/02-useEffect/SimpleForm';
// import { CounterApp } from './Components/01-useState/CcounterApp';
// import { CounterWithCustomHook } from './Components/01-useState/CounterWithCustomHook';
// import { HookApp } from './HookApp';
// import { Memorize } from './Components/06-memos/Memorize'

ReactDOM.render( 
  <CallbackHook />,
  document.getElementById('root')
);
