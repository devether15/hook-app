import React, { useRef, useLayoutEffect } from 'react'
import { useCounter } from '../../hooks/useCounter'
import { useFetch } from '../../hooks/useFetch'
import '../05-useLayoutEffect/layout.css'

export const LayoutEffect = () => {

    const {counter, increment} = useCounter(1);

    const { data} = useFetch(`https://www.breakingbadapi.com/api/quotes/1`);
    const { quote } = !!data && data[0];     

    const pTag = useRef()

    useLayoutEffect(() => {

        console.log(pTag.current.getBoundingClientRect())
        
        return () => {
            
        };
    }, [quote])

    return (
        <div>
            <h1>Layout Effect</h1>
            <hr/>

            <blockquote className="blockquote text-right">
                <p className='mb-0' ref={pTag}>{ quote }</p>            
            </blockquote>
      
            <button 
                lassName='btn btn-primary'
                onClick={increment}
            >
                Next Quote
            </button>       
        </div>
    )
}
